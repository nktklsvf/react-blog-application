import { useEffect, useState } from "react";
import { useLoading } from "./useLoading";

export const useLoadableContent = (contentGetter, callAfter = f => f, initialValue = undefined) => {
    const [content, setContent] = useState(initialValue);
    const [reupdate, setReupdate] = useState(false);
    const { setIsLoading } = useLoading();

    const update = () => {
        setReupdate(!reupdate);
    }

    useEffect(() => {
        setIsLoading(true);
        contentGetter().then(response => {
            setContent(response);
            callAfter(response);
            setIsLoading(false);
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [reupdate])

    return [
        content, update
    ]
}