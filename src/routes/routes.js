import Login from "../pages/login";
import Signup from "../pages/signup";
import EditProfile from "../pages/editProfile";
import User from "../pages/user";
import Users from "../pages/users";
import CreateArticle from "../pages/createArticle";
import Article from "../pages/article";
import EditArticle from "../pages/editArticle";
import Articles from "../pages/articles";
import CreateCategory from "../pages/createCategory";
import Category from "../pages/category";
import EditCategory from "../pages/editCategory";
import Categories from "../pages/categories";
import Home from "../pages/home";

export const routeLogin = "routeLogin";
export const routeSignup = "routeSignup";
export const routeUsersEdit = "routeUsersEdit";
export const routeUsersID = "routeUsersID";
export const routeUsers = "routeUsers";
export const routeArticlesCreate = "routeArticlesCreate";
export const routeArticlesID = "routeArticlesID";
export const routeArticlesEdit = "routeArticlesEdit";
export const routeArticles = "routeArticles";
export const routeCategoriesCreate = "routeCategoriesCreate";
export const routeCategoriesID = "routeCategoriesID";
export const routeCategoriesEdit = "routeCategoriesEdit";
export const routeCategories = "routeCategories";
export const routeHome = "routeHome";

export const routes = {
    [routeLogin]: {
        path: "/login",
        component: Login
    },

    [routeSignup]: {
        path: "/signup",
        component: Signup
    },

    [routeUsersEdit]: {
        path: `/users/:id/edit`,
        component: EditProfile,
        private: true
    },

    [routeUsersID]: {
        path: `/users/:id`,
        component: User
    },

    [routeUsers]: {
        path: "/users",
        component: Users
    },

    [routeArticlesCreate]: {
        path: '/articles/create/',
        component: CreateArticle,
        private: true
    },

    [routeArticlesEdit]: {
        path: `/articles/:id/edit`,
        component: EditArticle,
        private: true
    },

    [routeArticlesID]: {
        path: '/articles/:id',
        component: Article
    },

    [routeArticles]: {
        path: "/articles/",
        component: Articles
    },

    [routeCategoriesCreate]: {
        path: "/categories/create",
        component: CreateCategory,
        private: true
    },

    [routeCategoriesEdit]: {
        path: "/categories/:id/edit",
        component: EditCategory,
        private: true
    },

    [routeCategoriesID]: {
        path: "/categories/:id",
        component: Category
    },

    [routeCategories]: {
        path: "/categories",
        component: Categories
    },

    [routeHome]: {
        path: "/",
        component: Home
    }
};