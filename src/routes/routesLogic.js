import { useState } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import { routeLogin, routes } from "./routes";

export const FlashMessageTypes = {
  DANGER: 'danger',
  SUCCESS: 'success'
}

export const pushWithFlashMessage = (history, route, flashMessageType, flashMessage) => {
  history.push({ pathname: route, state: { flashMessage: { type: flashMessageType, message: flashMessage } } });
}

export const RedirectWithFlashMessage = ({ route, flashMessageType, flashMessage }) => {
  return (
    <Redirect
      to={{ pathname: route, state: { flashMessage: { type: flashMessageType, message: flashMessage } } }}
    />
  )
}

export const displayFlashMessage = (history, flashMessageType, flashMessage) => {
  pushWithFlashMessage(history, undefined, flashMessageType, flashMessage)
}

const PrivateRoute = props => {
  const { username, isLoaded } = useAuth();

  return (
    <>
      {isLoaded &&
        <>
          {username ?
            <Route
              {...props} />
            :
            <RedirectWithFlashMessage
              route={getRoute(routeLogin)}
              flashMessageType={FlashMessageTypes.DANGER}
              flashMessage="You must log in to perform this action!"
            />
          }
        </>
      }
    </>
  )
}

export const ExtendedSwitch = () => {
  const [routesKeys] = useState(Object.keys(routes));
  const [routesList] = useState(routes);
  return (
    <Switch>
      {routesKeys.map(item => routesList[item].private ? <PrivateRoute key={item} {...routesList[item]} /> : <Route key={item} {...routesList[item]} />)}
    </Switch>
  )
}

export const getRoute = (route, param) => param === undefined ? routes[route].path : routes[route].path.replace(param.param, param.value); 