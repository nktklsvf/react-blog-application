import { Link } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import { routeUsersEdit, routeUsersID } from "../../routes/routes";
import { getRoute } from "../../routes/routesLogic";
import APIService from "../../services/api.service";
import Cart from "./Cart";
import { EditButton } from "../Buttons/EditButton";
import { RemoveButton } from "../Buttons/RemoveButton";

function UserCart(props) {
    const { username, isAdmin } = useAuth();
    var canEdit = username === props.name;

    const onRemove = () => {
        if (window.confirm('Are you sure you want to delete the user account and all associated articles?')) {
            APIService.removeProfile(props.id).then((response) => {
                if (response.status) {
                    props.reupdate();
                }
            })
        }
    }

    return (
        <Cart
            header={
                <>
                    <Link className="mr-1" to={getRoute(routeUsersID, { param: ":id", value: props.id })}>{props.name}</Link>
                    {canEdit && (
                        <EditButton editLink={getRoute(routeUsersEdit, { param: ":id", value: props.id })} />
                    )}
                    {isAdmin && (
                        <RemoveButton onRemove={onRemove}/>
                    )}
                </>
            }
            title={
                <img width="150px" height="150px" alt={props.name} className="rounded shadow mx-auto d-block" src={props.avatar} />
            }
            text={
                props.articles_amount + " articles"
            }
            viewLink={
                getRoute(routeUsersID, { param: ":id", value: props.id })
            }
            footer={
                <small>{"Joined " + props.joined + " ago"}</small>
            }
        />
    )
}
export default UserCart;