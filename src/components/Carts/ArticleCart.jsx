import { Link } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import { routeArticlesEdit, routeArticlesID, routeCategoriesID, routeUsersID } from "../../routes/routes";
import { getRoute } from "../../routes/routesLogic";
import APIService from "../../services/api.service";
import Cart from "./Cart";
import { CartLink } from "./CartLink";
import { EditButton } from "../Buttons/EditButton";
import { RemoveButton } from "../Buttons/RemoveButton";

function ArticleCart(props) {

    const { username, isAdmin } = useAuth();

    const onRemoveArticle = e => {
        e.preventDefault()
        if (window.confirm('Are you sure?')) {
            APIService.removeArticle(props.id).then((response) => {
                if (response.status) {
                    props.reupdate();
                }
            })
        }
    }

    let canEdit = username === props.username || isAdmin;
    return (
        <Cart
            header={
                <>
                    by <Link to={getRoute(routeUsersID, { param: ":id", value: props.user_id })}>{props.username}</Link>
                </>
            }
            headerLink={
                props.category ? <CartLink title={props.category.name} to={getRoute(routeCategoriesID, { param: ":id", value: props.category.id })} /> : undefined
            }
            title={
                <>
                    <Link className="text-success mr-1" to={getRoute(routeArticlesID, { param: ":id", value: props.id })}>
                        {props.title}
                    </Link>
                    {canEdit && (
                        <>
                            <EditButton editLink={getRoute(routeArticlesEdit, { param: ":id", value: props.id })} />
                            <RemoveButton onRemove={onRemoveArticle} />
                        </>
                    )}
                </>
            }
            text={
                props.description
            }
            viewLink={
                getRoute(routeArticlesID, { param: ":id", value: props.id })
            }
            footer={
                <small>Created {props.created} ago, edited {props.updated} ago</small>
            }
        />
    )
}

export default ArticleCart;