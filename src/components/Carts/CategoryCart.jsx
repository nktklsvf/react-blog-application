import { Link } from "react-router-dom";
import { routeCategoriesID } from "../../routes/routes";
import { getRoute } from "../../routes/routesLogic";
import Cart from "./Cart";

function CategoryCart(props) {
    return (
        <Cart
            width={
                4
            }
            title={
                <Link className="text-success mr-1" to={getRoute(routeCategoriesID, { param: ":id", value: props.id })}>
                    {props.name}
                </Link>
            }
            text={
                props.articles_amount + " articles"
            }
            footer={
                <small>Created {props.created} ago, edited {props.updated} ago</small>
            }
        />
    )
}

export default CategoryCart;