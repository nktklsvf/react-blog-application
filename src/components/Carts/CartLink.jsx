import { Link } from "react-router-dom"

export const CartLink = ({ title, to }) => {
    return (
        <div className="mt-2">
            <Link className="badge badge-pill badge-info mr-1" to={to}>
                {title}
            </Link>
        </div>
    )
}