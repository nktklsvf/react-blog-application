import { Link } from "react-router-dom";

function Cart({ width = 8, header, headerLink, title, text, viewLink, footer }) {
  return (
    <div className="row justify-content-md-center">
      <div className={"col-" + width + " mt-4"}>
        <div className="card text-center shadow mb-5 bg-white rounded">
          {header &&
            <div className="card-header font-italic">
              {header}
              {headerLink}
            </div>
          }
          <div className="card-body">
            {title &&
              <h5 className="card-title mt-1">
                {title}
              </h5>
            }
            {text &&
              <>
                <div className="card-text">
                  {text}
                </div>
                <p></p>
              </>
            }
            {viewLink && <Link className="btn btn-outline-success mr-1" to={viewLink}>View</Link>}
          </div>
          {footer &&
            <div className="card-footer text-muted">
              {footer}
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default Cart;