export const Errors = ({ items, title, onCloseError }) => {
    return (
        <>
            {items.length > 0 &&
                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                    <h4 className="alert-heading">{title}</h4>
                    <ul>
                        {items.map((error, i) => <li key={i}>{error}</li>)}
                    </ul>
                    <button type="button" onClick={onCloseError} className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            }
        </>
    )
}