const Loading = () => {
    return (
      <>
        <div id="overlay"></div>
        <div id="loading">
          <div className="spinner-border" role="status"></div>
        </div>
      </>
    )
  }

export default Loading;