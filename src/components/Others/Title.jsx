function Title({title}) {
    return (
        <h1 className="text-center mt-4">
            {title}
        </h1>
    )
}

export default Title;