const Image = ({alt, ...props}) => {
    return (
      <img className="rounded shadow mx-auto d-block" alt={alt} {...props}/>
    )
  }

export default Image;