import { useEffect, useState } from "react";

export const SearchBar = ({ onChange = f => f, searchSize = 8 }) => {

    const [input, setInput] = useState("");
    const [inputTimeout, setInputTimeout] = useState()

    const onChangeInput = e => {
        if (inputTimeout) {
            clearTimeout(inputTimeout);
        }
        setInputTimeout(setTimeout(() => {
            setInput(e.target.value)
        }, 500));
    }

    useEffect(() => {
        onChange(input)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [input])

    return (
        <div className="row justify-content-md-center">
            <div className={`col-${searchSize} mt-4`}>
                <div className="input-group rounded">
                    <input type="text" className="form-control rounded" placeholder="Search" aria-label="Search" onChange={onChangeInput} />
                </div>
            </div>
        </div>
    )
}