import { useLoading } from "../../hooks/useLoading";
import { SearchBar } from "./SearchBar";
import Title from "./Title";


function List({ title, items = [], Component, reupdateList = f => f, filter = f => f, searchSize = 8 }) {
    const { isLoading } = useLoading();

    return (
        <>
            {!isLoading &&
                <>
                    <Title title={title} />
                    <div>
                        <div className="flickr_pagination"></div>
                        <div className="container">
                            <SearchBar onChange={input => filter(input)} searchSize={searchSize} />
                            {items.map(item => <Component key={item.id} reupdate={reupdateList} {...item} />)}
                        </div>
                    </div>
                </>
            }
        </>
    )
}

export default List;