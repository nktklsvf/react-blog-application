import { Link } from "react-router-dom";
import { useRef } from "react";

export const FileUploaderButton = ({ onFileChange, text }) => {
    const fileUploadInput = useRef();
  
    const triggerInput = e => {
      e.preventDefault()
      fileUploadInput.current.click()
    }
  
    const onChange = e => {
      const file = e.target.files[0]
      if (file) {
        onFileChange(file)
      }
    }
  
    return (
      <>
        <input type="file" hidden ref={fileUploadInput} onChange={onChange} />
        <Link
          className="btn btn-outline-info mr-1"
          to={"/href"}
          onClick={triggerInput}
        >
          {text}
        </Link>
      </>
    )
  }