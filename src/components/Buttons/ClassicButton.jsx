import { Link } from "react-router-dom"

export const ClassicButton = ({ to, text }) => {
    return (
        <Link
            className="btn btn-outline-info mr-1"
            to={to}
        >
            {text}
        </Link>
    )
}