function FormInputSelect({label, children, id, ...props}) {
    return (
    <div className="form-group row">
        <label className="col-2 col-form-label text-light">{label}</label>
        <div className="col-10"> 
            <input name={id} type="hidden" value="" />
            <select multiple={false} size="3" className="custom-select shadow rounded" id={id} {...props}>
                {children}
            </select>
        </div>
    </div>
    )
}

export default FormInputSelect;