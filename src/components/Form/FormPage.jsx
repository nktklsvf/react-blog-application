import { Link } from "react-router-dom";
import { routeArticles } from "../../routes/routes";
import { getRoute } from "../../routes/routesLogic";
import { Errors } from "../Others/Errors";

const FormPage = ({ errors = [], errorTitle = "", onCloseError = f => f, children }) => {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-10">
                    <Errors items={errors} title={errorTitle} onCloseError={onCloseError}/>
                    {children}
                </div>
                <div className="mb-3">
                    <Link className="text-info" to={getRoute(routeArticles)}>[ Cancel and return to articles listing ]</Link>
                </div>
            </div>
        </div>
    )
}

export default FormPage;