function FormSubmit({text}) {
    return (
        <div className="form-group row justify-content-center">
            <input type="submit" name="commit" value={text} className="btn btn-outline-light btn-lg" data-disable-with={text}/>
        </div>
    )
}

export default FormSubmit;