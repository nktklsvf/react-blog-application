function Form({children, handleSubmit}) {
    return (
        <form className="shadow p-3 mb-3 bg-info rounded" onSubmit={handleSubmit}>
            {children}
        </form>    
    )
}

export default Form;