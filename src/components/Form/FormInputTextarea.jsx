function FormInputTextarea({label, rows, ...props}) {
    return (
        <div className="form-group row">
            <label className="col-2 col-form-label text-light">{label}</label>
            <div className="col-10"> 
                <textarea rows={rows} className="form-control shadow rounded" {...props}/>
            </div>
        </div>
    )   
}

export default FormInputTextarea;