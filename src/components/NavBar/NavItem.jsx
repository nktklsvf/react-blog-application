import { Link } from "react-router-dom"

export const NavItem = ({ name, link, dropdown = false, children, ...props }) => {
    return (
      <li className={dropdown ? "nav-item dropdown" : "nav-item"}>
        {dropdown ?
          <>
            <a className="nav-link dropdown-toggle" href="/link" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {` ${name} `}
            </a>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              {children}
            </div>
          </>
          :
          <Link className="nav-link" to={link} {...props}>
            {name}
          </Link>
        }
      </li>
    )
  }