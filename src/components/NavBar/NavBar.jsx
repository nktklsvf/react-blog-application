import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { useAuth } from "../../hooks/useAuth";
import { routeArticles, routeArticlesCreate, routeCategories, routeCategoriesCreate, routeHome, routeLogin, routeSignup, routeUsers, routeUsersEdit, routeUsersID } from "../../routes/routes";
import { FlashMessageTypes, getRoute, pushWithFlashMessage } from "../../routes/routesLogic";
import { NavBarToggler } from "./NavBarToggler";
import { NavDropdownItem } from "./NavDropdownItem";
import { NavItem } from "./NavItem";

const NavBar = ({ name }) => {
  const { id, username, isAdmin, isLoaded, logout, remove } = useAuth();
  const history = useHistory();

  const onRemoveProfile = e => {
    e.preventDefault();
    if (window.confirm('Are you sure you want to delete the user account and all associated articles?')) {
      remove(() => history.push(getRoute(routeHome)));
    };
  }

  const onLogout = e => {
    e.preventDefault();
    logout();
    pushWithFlashMessage(history, '/', FlashMessageTypes.SUCCESS, 'Logged out');
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" id="logo" to={getRoute(routeHome)}>{name}</Link>
      <NavBarToggler />
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          {isLoaded &&
            <>
              <NavItem name="Bloggers" link={getRoute(routeUsers)} />

              <NavItem name="Articles" dropdown={true}>
                {username && <NavDropdownItem name="Create new article" link={getRoute(routeArticlesCreate)} />}
                <NavDropdownItem name="View articles" link={getRoute(routeArticles)} />
              </NavItem>

              <NavItem name="Categories" dropdown={true}>
                {username && <NavDropdownItem name="Create new category" link={getRoute(routeCategoriesCreate)} />}
                <NavDropdownItem name="View categories" link={getRoute(routeCategories)} />
              </NavItem>

              {username ?
                <>
                  <NavItem name={(isAdmin ? "(Admin) " : "") + "Profile [" + username + "] "} dropdown={true}>
                    <NavDropdownItem
                      name="View profile"
                      link={getRoute(routeUsersID, { param: ":id", value: id })} />
                    <NavDropdownItem
                      name="Edit profile"
                      link={getRoute(routeUsersEdit, { param: ":id", value: id })} />
                    <NavDropdownItem
                      name="Delete profile"
                      link={getRoute(routeUsersEdit, { param: ":id", value: id })}
                      danger={true}
                      onClick={onRemoveProfile} />
                  </NavItem>
                  <NavItem name="Logout" link={"/link"} onClick={onLogout} />
                </>
                :
                <>
                  <NavItem name="Log in" link={getRoute(routeLogin)} />
                  <NavItem name="Sign up" link={getRoute(routeSignup)} />
                </>
              }
            </>
          }

        </ul>
      </div>
    </nav>
  )

}

export default NavBar;