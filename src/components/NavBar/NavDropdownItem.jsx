import { Link } from "react-router-dom"

export const NavDropdownItem = ({ danger, link, name, ...props }) => {
    return (
      <Link
        className={danger ? "dropdown-item text-danger" : "dropdown-item"}
        to={link}
        {...props}>
        {name}
      </Link>
    )
  }