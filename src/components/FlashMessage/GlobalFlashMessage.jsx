import { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { FlashMessage } from "./FlashMessage";

export const GlobalFlashMessage = () => {

    const [flashMessage, setFlashMessage] = useState();
    const location = useLocation();
  
      useEffect(() => {
          setFlashMessage(
            location && location.state ? {
              ...location.state.flashMessage
          } : {
              type: undefined, 
              message: ""   
          }
          )
      }, [location])
  
    return (
      <>
          {flashMessage && flashMessage.type && (
            <FlashMessage {...flashMessage} onClose={() => setFlashMessage({...flashMessage, type : undefined})}/>
          )}
      </>
    )
  }