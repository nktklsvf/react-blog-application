export const FlashMessage = ({ type, message, onClose }) => {
    return (
        <>
            <div className={`alert alert-${type} alert-dismissible fade show`} role="alert">
                {message}
                <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={e => {
                    e.preventDefault();
                    onClose();
                }}>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </>
    )
}