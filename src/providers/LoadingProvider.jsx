import React, { useState } from "react";
import Loading from "../components/Others/Loading";
import { LoadingContext } from "../hooks/useLoading";

export const LoadingProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);
  return (
    <LoadingContext.Provider value={{ isLoading, setIsLoading }}>
      {isLoading && <Loading />}
      {children}
    </LoadingContext.Provider>
  )
}