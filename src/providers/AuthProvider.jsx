import React, { useEffect, useState } from "react";
import { AuthContext } from "../hooks/useAuth";
import APIService from "../services/api.service";

export const AccessToken = "access_token";

export const AuthProvider = ({ children }) => {
    const [data, setData] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);
    const [update, setUpdate] = useState(false);
    const forceUpdate = () => setUpdate(!update);

    useEffect(() => {
        APIService.getInfo().then(response => {
            if (response.username) {
                const { id, username, isAdmin } = response;
                setData({ ...data, id, username, isAdmin });

            } else {
                setData({});
            }
            setIsLoaded(true);
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [update])

    const logout = () => {
        localStorage.removeItem(AccessToken);
        setData({});
        forceUpdate();
    }

    const login = (username, password, onSuccess = f => f, onFail = f => f) => {
        APIService.login(username, password).then(response => {
            if (response.access_token !== undefined) {
                localStorage.setItem(AccessToken, response.access_token);
                forceUpdate();
                onSuccess(response.id);
            } else {
                onFail();
            }
        });
    }

    const signup = (username, password, email, onSuccess = f => f, onFail = f => f) => {
        APIService.register(username, password, email).then(response => {
            if (response.access_token !== undefined) {
                localStorage.setItem(AccessToken, response.access_token);
                forceUpdate();
                onSuccess();
            } else {
                onFail(response.errors);
            }
        });
    }

    const remove = (onSuccess = f => f, onFail = f => f) => {
        APIService.removeProfile(data.id).then(() => {
            logout();
            onSuccess();
        })
    }

    return (
        <AuthContext.Provider value={{ ...data, isLoaded, logout, login, signup, remove }}>
            {children}
        </AuthContext.Provider>
    )

}