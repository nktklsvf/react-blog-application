import axios from "axios";
import { AccessToken } from "../providers/AuthProvider";

const API_URL = "http://localhost:3000/"

const requestTypes = {
    POST: 'POST',
    GET: 'GET',
    PATCH: 'PATCH',
    DELETE: 'DELETE'
}

const send = (type, path, params, useAccessToken = false) => {
    if (useAccessToken) {
        let accessToken = localStorage.getItem(AccessToken);
        if (accessToken) {
            if (params instanceof FormData) {
                params.append(AccessToken, accessToken)
            } else {
                params = {
                    ...params,
                    access_token: accessToken
                }
            }
        }
    }
    let ret;
    if (type === requestTypes.GET) {
        ret = axios.get(path)
    } else if (type === requestTypes.POST) {
        ret = axios.post(path, params);
    } else if (type === requestTypes.PATCH) {
        ret = axios.put(path, params)
    } else if (type === requestTypes.DELETE) {
        ret = axios.delete(path, {
            headers: {
            },
            data: {
                ...params
            }
        });
    }
    return ret.then(response => response.data);
}

const register = (username, password, email) => {
    return send(requestTypes.POST, API_URL + "signup", { username, password, email })
}

const login = (email, password) => {
    return send(requestTypes.POST, API_URL + "login", { email, password })
}

const getInfo = () => {
    return send(requestTypes.POST, API_URL + "info", {}, true);
}

const getUser = user_id => {
    return send(requestTypes.GET, API_URL + "users/" + user_id);
}

const createArticle = (title, description, category_id) => {
    return send(requestTypes.POST, API_URL + "articles/", { title, description, category_id: category_id ? category_id : null }, true)
}

const getArticle = (id) => {
    return send(requestTypes.GET, API_URL + "articles/" + id);
}

const getAllArticles = (userID) => {
    return send(requestTypes.GET, API_URL + "articles/" + (userID ? "?user_id=" + userID : ""));

}

const getAllArticlesByCategory = (categoryID) => {
    return send(requestTypes.GET, API_URL + "articles/" + (categoryID ? "?category_id=" + categoryID : ""));
}

const getAllBloggers = () => {
    return send(requestTypes.GET, API_URL + "users/");
}

const getAllCategories = () => {
    return send(requestTypes.GET, API_URL + "categories/");
}

const getProfileInfo = userID => {
    return send(requestTypes.GET, API_URL + "edit/" + userID);
}

const updateProfile = (userID, username, email, password) => {
    return send(requestTypes.PATCH, API_URL + "users/" + userID, { username, email, password }, true);
}

const updateAvatar = (userID, file) => {
    const formData = new FormData();
    formData.append( 
        "avatar", 
        file, 
        file.name
    ); 
    return send(requestTypes.POST, API_URL + "users/avatar/" + userID, formData, true)
}

const createCategory = (name) => {
    return send(requestTypes.POST, API_URL + "categories/", { name }, true);
}

const updateCategory = (id, name) => {
    return send(requestTypes.PATCH, API_URL + "categories/" + id, { id, name }, true);
}

const getCategory = (id) => {
    return send(requestTypes.GET, API_URL + "categories/" + id);
}

const removeProfile = (id) => {
    return send(requestTypes.DELETE, API_URL + "users/" + id, { id }, true);
}

const removeArticle = (id) => {
    return send(requestTypes.DELETE, API_URL + "articles/" + id, {}, true)
}

const updateArticle = (id, title, description, category_id) => {
    return send(requestTypes.PATCH, API_URL + "articles/" + id, { id, title, description, category_id: (category_id !== '' ? category_id : null) }, true);
}

const exportList = {
    register,
    login,
    getInfo,
    getUser,
    createArticle,
    getArticle,
    getAllArticles,
    getAllArticlesByCategory,
    getAllBloggers,
    getAllCategories,
    getProfileInfo,
    updateProfile,
    updateAvatar,
    createCategory,
    updateCategory,
    getCategory,
    removeProfile,
    removeArticle,
    updateArticle
}

export default exportList;