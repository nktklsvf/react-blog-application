import { useState } from "react";
import { useParams } from "react-router";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormInputSelect from "../components/Form/FormInputSelect";
import FormInputTextarea from "../components/Form/FormInputTextarea";
import FormSubmit from "../components/Form/FormSubmit";
import FormPage from "../components/Form/FormPage";
import Title from "../components/Others/Title";
import { useInput } from "../hooks/useInput";
import { useLoadableContent } from "../hooks/useLoadableContent";
import APIService from "../services/api.service";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { getRoute } from "../routes/routesLogic";
import { routeArticles } from "../routes/routes";
import { useLoading } from "../hooks/useLoading";

const EditArticle = () => {
    const history = useHistory();
    const [title, , setTitle] = useInput("");
    const [description, , setDescription] = useInput("");
    const [category, , setCategory] = useInput("");
    const [categoryList, setCategoryList] = useState(undefined);
    const [errors, setErrors] = useState([]);
    const articleID = useParams().id;
    const {setIsLoading} = useLoading();

    const formInit = (content) => {
        if (content.id) {
            setTitle(content.title);
            setDescription(content.description);
            if (content.category) {
                setCategory(content.category.id);
            }
        }
    }

    useLoadableContent(() => APIService.getAllCategories(), (content) => setCategoryList(content), {});
    useLoadableContent(() => APIService.getArticle(articleID), formInit, {})

    const handleSubmit = e => {
        e.preventDefault();
        setIsLoading(true);
        APIService.updateArticle(articleID, title.value, description.value, category.value).then((response) => {
            if (response.errors) {
                setErrors([...response.errors]);
                setIsLoading(false);
            } else {
                history.push(getRoute(routeArticles));
            }
        })
    }

    return (
        <>
            {categoryList &&
                <>
                    <Title title="Edit article" />
                    <FormPage 
                        errors={errors}
                        errorTitle="The following errors prevented the article from being saved"
                        onCloseError={() => setErrors([])}
                        >
                        <Form handleSubmit={handleSubmit}>
                            <FormInput label="Title" placeholder="Title of article" value={title} type="text" {...title} />
                            <FormInputTextarea label="Description" rows="10" value={description} placeholder="Description of article" {...description} />
                            <FormInputSelect label="Category" name="category_id" id="category_id" {...category}>
                                <option value="">Make your selection from the list below (can be empty)</option>
                                {categoryList.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
                            </FormInputSelect>
                            <FormSubmit text="Update Article" />
                        </Form>
                    </FormPage>
                </>
            }
        </>
    )
}

export default EditArticle;