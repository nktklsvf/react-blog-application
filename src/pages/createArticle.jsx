import { useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormInputSelect from "../components/Form/FormInputSelect";
import FormInputTextarea from "../components/Form/FormInputTextarea";
import FormPage from "../components/Form/FormPage";
import FormSubmit from "../components/Form/FormSubmit";
import Title from "../components/Others/Title";
import { useInput } from "../hooks/useInput";
import { useLoadableContent } from "../hooks/useLoadableContent";
import { useLoading } from "../hooks/useLoading";
import { routeArticles } from "../routes/routes";
import { getRoute } from "../routes/routesLogic";
import APIService from "../services/api.service";

const CreateArticle = () => {

    const [title] = useInput("");
    const [description] = useInput("");
    const [category] = useInput("");
    const [categoryList, setCategoryList] = useState(undefined);
    const [errors, setErrors] = useState([]);
    const history = useHistory();
    const { setIsLoading } = useLoading();

    function handleSubmit(e) {
        e.preventDefault();
        setIsLoading(true);
        APIService.createArticle(title.value, description.value, category.value).then((response) => {
            if (response.errors) {
                setErrors([...response.errors]);
                setIsLoading(false);
            } else {
                history.push(getRoute(routeArticles, { param: ":id", value: response.article_id }));
            }
        })
    }

    useLoadableContent(() => APIService.getAllCategories(), (content) => setCategoryList(content), {});

    return (
        <>
            {categoryList &&
                <>
                    <Title title="Create a new article" />
                    <FormPage
                        errors={errors}
                        errorTitle="The following errors prevented the article from being saved"
                        onCloseError={() => setErrors([])}>

                        <Form handleSubmit={handleSubmit}>
                            <FormInput label="Title" placeholder="Title of article" type="text" {...title} />
                            <FormInputTextarea label="Description" rows="10" placeholder="Description of article" {...description} />
                            <FormInputSelect label="Category" {...category}>
                                <option value="">Make your selection from the list below (can be empty)</option>
                                {categoryList.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
                            </FormInputSelect>
                            <FormSubmit text="Create Article" />
                        </Form>
                    </FormPage>
                </>
            }
        </>
    )
}

export default CreateArticle;