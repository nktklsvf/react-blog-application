import React from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { EditButton } from '../components/Buttons/EditButton';
import { RemoveButton } from '../components/Buttons/RemoveButton';
import Cart from '../components/Carts/Cart';
import { CartLink } from '../components/Carts/CartLink';
import Image from '../components/Others/Image';
import Title from '../components/Others/Title';
import { useAuth } from '../hooks/useAuth';
import { useLoadableContent } from '../hooks/useLoadableContent';
import { routeArticles, routeArticlesEdit, routeCategories, routeUsersID } from '../routes/routes';
import { getRoute } from '../routes/routesLogic';
import APIService from '../services/api.service';

const Article = () => {
    const history = useHistory();
    const { id, username, isAdmin } = useAuth();
    const articleID = useParams().id;
    const [response] = useLoadableContent(() => APIService.getArticle(articleID), f => f, undefined);
    const canEdit = response ? isAdmin || (username === response.username) : false;

    const onRemoveArticle = () => {
        if (window.confirm('Are you sure?')) {
            APIService.removeArticle(articleID).then(() => {
                history.push(getRoute(routeArticles))
            })
        }
    };
    return (
        <>
            {response &&
                <>
                    <Title title={
                        <>
                            <span className='mr-1'>{response.title}</span>
                            {canEdit && (
                                <>
                                    <EditButton editLink={getRoute(routeArticlesEdit, { param: ":id", value: response.id })} size={24} />
                                    <RemoveButton onRemove={onRemoveArticle} size={24} />
                                </>
                            )}
                        </>
                    } />
                    <div className="container">
                        <Cart
                            header={
                                <>
                                    by {response.username}
                                    <Link to={getRoute(routeUsersID, { param: ":id", value: response.user_id })}>
                                        <Image
                                            alt={response.username}
                                            src={response.user_avatar}
                                            width="80px"
                                            height="80px" />
                                    </Link>
                                </>}
                            headerLink={response.category ?
                                <CartLink title={response.category.name} to={getRoute(routeCategories, { param: ":id", value: response.category.id })} />
                                : undefined}
                            text={
                                <div className="card-text text-left">
                                    <p>{response.description}</p>
                                </div>}

                            footer={<small>Created {response.created} ago, edited {response.updated} ago</small>}
                        />
                    </div>
                </>
            }
        </>
    )
}

export default Article;