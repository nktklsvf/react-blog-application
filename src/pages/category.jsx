import { useParams } from "react-router";
import { Link } from "react-router-dom";
import Title from "../components/Others/Title";
import { useLoadableContent } from "../hooks/useLoadableContent";
import { useAuth } from "../hooks/useAuth";
import APIService from "../services/api.service";
import Articles from "./articles";
import { getRoute } from "../routes/routesLogic";
import { routeCategoriesEdit } from "../routes/routes";

const Category = () => {
    let id = useParams().id;
    const { isAdmin } = useAuth();
    const [category] = useLoadableContent(() => APIService.getCategory(id));
    return (
        <>
            {category &&
                <>
                    <Title title={`Category: ${category.name}`} />
                    {isAdmin &&
                        <h1 className="text-center mt-4">
                            <Link className="btn btn-outline-info" to={getRoute(routeCategoriesEdit, {param: ":id", value: id})}>
                                Edit category name
                            </Link>
                        </h1>
                    }
                    <Articles categoryID={id} />
                </>
            }
        </>
    )
}

export default Category;