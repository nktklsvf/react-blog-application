import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { ClassicButton } from "../components/Buttons/ClassicButton";
import { FileUploaderButton } from "../components/Buttons/FileUploaderButton";
import { Errors } from "../components/Others/Errors";
import Image from "../components/Others/Image";
import Title from "../components/Others/Title";
import { useAuth } from "../hooks/useAuth";
import { useLoading } from "../hooks/useLoading";
import { routeUsersEdit } from "../routes/routes";
import { getRoute } from "../routes/routesLogic";
import APIService from "../services/api.service";
import Articles from "./articles";

const User = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  const userID = useParams().id;
  const [user, setUser] = useState({});
  const { username } = useAuth();
  const [update, setUpdate] = useState(false);
  const { setIsLoading } = useLoading();
  const [errors, setErrors] = useState([])

  const forceUpdate = () => setUpdate(!update)

  useEffect(() => {
    APIService.getUser(userID).then((response) => {
      if (response.username) {
        setIsLoaded(true);
        setUser(response);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [update]);

  const onFileChange = file => {
    setIsLoading(true)
    setErrors([])
    APIService.updateAvatar(userID, file).then(response => {
      if (response.errors) {
        setErrors([...response.errors])
      } else {
        forceUpdate()
      }
      setIsLoading(false)
    })
  }

  return (
    <>
      {isLoaded && (
        <>
          <Errors items={errors} title="The following errors prevented the avatar from being uploaded" onCloseError={() => setErrors([])} />
          <Title title={user.username + "'s profile"} />
          <Image
            alt={user.username}
            width="150px"
            height="150px"
            className="rounded shadow mx-auto d-block"
            src={user.avatar}
          />
          {username === user.username && (
            <h1 className="text-center mt-4">
              <ClassicButton to={getRoute(routeUsersEdit, { param: ":id", value: user.id })} text="Edit your profile" />
              <FileUploaderButton onFileChange={onFileChange} text="Update avatar" />
            </h1>
          )}
        </>
      )}
      <Articles userID={userID} />
    </>
  );
};

export default User;
