import { useState } from "react";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormSubmit from "../components/Form/FormSubmit";
import FormPage from "../components/Form/FormPage";
import Title from "../components/Others/Title";
import { useInput } from "../hooks/useInput";
import { routeCategoriesID } from "../routes/routes";
import { getRoute } from "../routes/routesLogic";
import APIService from "../services/api.service";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { useLoading } from "../hooks/useLoading";

const CreateCategory = () => {
    const history = useHistory();
    const [category] = useInput("");
    const [errors, setErrors] = useState([]);
    const {setIsLoading} = useLoading();

    const handleSubmit = e => {
        e.preventDefault();
        setIsLoading(true);
        APIService.createCategory(category.value).then((response) => {
            if (response.errors) {
                setErrors([...response.errors]);
                setIsLoading(false);
            }
            else {
                history.push(getRoute(routeCategoriesID, { param: ":id", value: response.category_id }));
            }
        })
    }

    return (
        <>
            <Title title="Create a new category" />
            <FormPage
                errors={errors}
                errorTitle="The following errors prevented the category from being saved"
                onCloseError={() => setErrors([])}
            >
                <Form handleSubmit={handleSubmit}>
                    <FormInput label="Category name" placeholder="Enter a name" type="text" {...category} />
                    <FormSubmit text="Create Category" />
                </Form>
            </FormPage>
        </>
    )
}

export default CreateCategory;