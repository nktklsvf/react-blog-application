import React from "react";
import CategoryCart from "../components/Carts/CategoryCart";
import List from "../components/Others/List";
import { useLoadableContent } from "../hooks/useLoadableContent";
import APIService from "../services/api.service";
import { useState } from "react";


const Categories = () => {
    const [filteredCategoeies, setFilteredCategories] = useState([])
    const [categories] = useLoadableContent(
        () => APIService.getAllCategories(),
        categories => setFilteredCategories(categories),
        []);

    const filter = input => {
        setFilteredCategories(categories.filter(categories => categories.name.toLowerCase().includes(input.toLowerCase())))
    }

    return (
        <List title="Categories" items={filteredCategoeies} Component={CategoryCart} filter={filter} searchSize={4}/>
    )
}

export default Categories;