import React, { useState } from 'react';
import ArticleCart from '../components/Carts/ArticleCart';
import List from '../components/Others/List';
import { useLoadableContent } from '../hooks/useLoadableContent';
import APIService from '../services/api.service';

function Articles({ userID, categoryID }) {
  const [filteredArticles, setFilteredArticles] = useState([])
  const [articles, reupdateArticles] = useLoadableContent(
    userID ? () => APIService.getAllArticles(userID) : () => APIService.getAllArticlesByCategory(categoryID),
    articles => setFilteredArticles(articles),
    []);

  const filter = input => {
    setFilteredArticles(articles.filter(article => article.title.toLowerCase().includes(input.toLowerCase())))
  }

  return (
    <>
      <List title="Articles" reupdateList={reupdateArticles} items={filteredArticles} Component={ArticleCart} filter={filter} />
    </>
  )
}

export default Articles;