import { Link } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import { Redirect } from "react-router-dom";
import { getRoute } from "../routes/routesLogic";
import { routeArticles, routeSignup } from "../routes/routes";

const Home = () => {
  const { username, isLoaded } = useAuth();
  return (
    <>
      {isLoaded && (
        <>
          {!username ? (
            <div className="container" id="home-container">
              <div className="jumbotron text-center text-white">
                <h1 className="display-4">Alpha Blog</h1>
                <Link className="btn btn-success btn-lg" to={getRoute(routeSignup)}>
                  Sign up!
                </Link>
              </div>
            </div>
          ) : (
            <Redirect
              to={{
                pathname: getRoute(routeArticles),
              }}
            />
          )}
        </>
      )}
    </>
  );
};

export default Home;
