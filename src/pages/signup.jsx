import React, { useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormPage from "../components/Form/FormPage";
import FormSubmit from "../components/Form/FormSubmit";
import Title from "../components/Others/Title";
import { useAuth } from "../hooks/useAuth";
import { useInput } from "../hooks/useInput";
import { useLoading } from "../hooks/useLoading";
import { routeUsers } from "../routes/routes";
import { getRoute } from "../routes/routesLogic";

const Signup = () => {
    const history = useHistory();
    const [login] = useInput("");
    const [email] = useInput("");
    const [password] = useInput("");
    const { signup } = useAuth();
    const { setIsLoading } = useLoading();
    const [errors, setErrors] = useState([]);

    const handleSignup = (e) => {
        e.preventDefault();
        setIsLoading(true);
        signup(
            login.value,
            password.value,
            email.value,
            () => history.push(getRoute(routeUsers)),
            errors => {
                setErrors([...errors]);
                setIsLoading(false);
            }
        );
    };

    return (
        <>
            <Title title="Sign up" />
            <FormPage
                errors={errors}
                errorTitle="The following errors prevented the user from being saved"
                onCloseError={() => setErrors([])}
            >

                <Form handleSubmit={handleSignup}>
                    <FormInput
                        label="Username"
                        placeholder="Enter a username"
                        {...login}
                    />
                    <FormInput
                        label="Email"
                        placeholder="Enter your email address"
                        {...email}
                    />
                    <FormInput
                        label="Password"
                        placeholder="Choose a password"
                        type="password"
                        {...password}
                    />
                    <FormSubmit text="Sign up" />
                </Form>
            </FormPage>
        </>
    );
};

export default Signup;
