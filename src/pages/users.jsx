import UserCart from "../components/Carts/UserCart";
import List from "../components/Others/List";
import { useLoadableContent } from "../hooks/useLoadableContent";
import APIService from "../services/api.service";
import { useState } from "react";


const Users = () => {
  const [filteredBloggers, setFilteredBloggers] = useState([])
  const [bloggers, reupdateBloggers] = useLoadableContent(
    () => APIService.getAllBloggers(),
    bloggers => setFilteredBloggers(bloggers),
    []);

  const filter = input => {
    setFilteredBloggers(bloggers.filter(blogger => blogger.name.toLowerCase().includes(input.toLowerCase())))
  }

  return (
    <List title="Bloggers" items={filteredBloggers} Component={UserCart} reupdateList={reupdateBloggers} filter={filter}/>
  )
}

export default Users;