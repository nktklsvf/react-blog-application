import React, { useState } from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormSubmit from "../components/Form/FormSubmit";
import FormPage from "../components/Form/FormPage";
import Title from "../components/Others/Title";
import { useInput } from "../hooks/useInput";
import { useLoadableContent } from "../hooks/useLoadableContent";
import { routeUsersID } from "../routes/routes";
import { getRoute } from "../routes/routesLogic";
import APIService from "../services/api.service";
import { useLoading } from "../hooks/useLoading";

const EditProfile = () => {

    const history = useHistory();
    const profileID = useParams().id;
    const [username, , setUsername] = useInput("");
    const [email, , setEmail] = useInput("");
    const [password] = useInput("");
    const [errors, setErrors] = useState([]);
    const {setIsLoading} = useLoading();

    const updateInfo = response => {
        setUsername(response.username);
        setEmail(response.email);
    }
    useLoadableContent(() => APIService.getProfileInfo(profileID), updateInfo);

    const handleSubmit = e => {
        e.preventDefault();
        setIsLoading(true);
        APIService.updateProfile(profileID, username.value, email.value, password.value).then(response => {
            if (response.errors) {
                setErrors([...response.errors]);
                setIsLoading(false);
            } else {
                history.push(getRoute(routeUsersID, { param: ":id", value: profileID }));
            }
        })
    }

    return (
        <>
            <Title title="Edit your profile" />
            <FormPage
                errors={errors}
                errorTitle="The following errors prevented the user from being saved"
                onCloseError={() => setErrors([])}>
                <Form handleSubmit={handleSubmit}>
                    <FormInput label="Username" placeholder="Enter a username" type="text" {...username} />
                    <FormInput label="Email" placeholder="Enter your email address" type="email" {...email} />
                    <FormInput label="Password" placeholder="Enter a password" type="password" {...password} />
                    <FormSubmit text="Update account" />
                </Form>
            </FormPage>
        </>
    )
}

export default EditProfile;