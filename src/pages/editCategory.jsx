import { useState } from "react";
import { useParams } from "react-router";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormSubmit from "../components/Form/FormSubmit";
import FormPage from "../components/Form/FormPage";
import Title from "../components/Others/Title";
import { useInput } from "../hooks/useInput";
import { useLoadableContent } from "../hooks/useLoadableContent";
import { routeCategoriesID } from "../routes/routes";
import { getRoute } from "../routes/routesLogic";
import APIService from "../services/api.service";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { useLoading } from "../hooks/useLoading";

const EditCategory = () => {
    const history = useHistory();
    const [category, , setCategory] = useInput("");
    const [errors, setErrors] = useState([]);
    const {setIsLoading} = useLoading();

    var id = useParams().id;
    useLoadableContent(() => APIService.getCategory(id), response => setCategory(response.name));

    const handleSubmit = e => {
        e.preventDefault();
        setIsLoading(true);
        APIService.updateCategory(id, category.value).then((response) => {
            if (response.errors) {
                setErrors(response.errors);
                setIsLoading(false);
            } else {
                history.push(getRoute(routeCategoriesID, { param: ":id", value: response.category_id }));
            }
        })
    }

    return (
        <>
            <Title title="Edit category name" />
            <FormPage
                errors={errors}
                errorTitle="The following errors prevented the category from being saved"
                onCloseError={() => setErrors([])}>
                <Form handleSubmit={handleSubmit}>
                    <FormInput label="Category name" placeholder="Enter a name" type="text" {...category} />
                    <FormSubmit text="Update Category" />
                </Form>
            </FormPage>
        </>
    )
}

export default EditCategory;