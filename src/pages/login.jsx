import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Form from "../components/Form/Form";
import FormInput from "../components/Form/FormInput";
import FormPage from "../components/Form/FormPage";
import FormSubmit from "../components/Form/FormSubmit";
import Title from "../components/Others/Title";
import { useAuth } from "../hooks/useAuth";
import { useInput } from "../hooks/useInput";
import { useLoading } from "../hooks/useLoading";
import { routeUsers } from "../routes/routes";
import { displayFlashMessage, FlashMessageTypes, getRoute, pushWithFlashMessage } from "../routes/routesLogic";

const Login = () => {
    const history = useHistory();
    const [username] = useInput("");
    const [password] = useInput("");
    const { login } = useAuth();
    const { setIsLoading } = useLoading();

    const handleLogin = e => {
        e.preventDefault();
        setIsLoading(true);
        login(username.value,
            password.value,
            id => pushWithFlashMessage(
                history,
                getRoute(routeUsers, { param: ":id", value: id }),
                FlashMessageTypes.SUCCESS, 'Logged in successfully'),

            () => {
                displayFlashMessage(history,
                    FlashMessageTypes.DANGER,
                    'Something is wrong with your log in details!');
                setIsLoading(false);
            }
        );
    }

    return (
        <div className="container">
            <Title title="Log in" />
            <FormPage>
                <Form handleSubmit={handleLogin}>
                    <FormInput label="Email" placeholder="Enter your email address" type="email" {...username} />
                    <FormInput label="Password" placeholder="Enter your password" type="password" {...password} />
                    <FormSubmit text="Log in" />
                </Form>
            </FormPage>
        </div>
    )
}


export default Login;