import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { createBrowserHistory } from "history";
import React from 'react';
import { Router } from "react-router-dom";
import './App.css';
import { GlobalFlashMessage } from "./components/FlashMessage/GlobalFlashMessage";
import NavBar from './components/NavBar/NavBar';
import { Footer } from './components/Others/Footer';
import { LoadingProvider } from "./providers/LoadingProvider";
import { ExtendedSwitch } from "./routes/routesLogic";
const history = createBrowserHistory();

const App = () => {
  return (
    <Router history={history}>
      <div id="page-content">
        <NavBar name="Blog" />
        <div className="container">
          <GlobalFlashMessage />
          <LoadingProvider>
            <ExtendedSwitch />
          </LoadingProvider>
        </div>
      </div>
      <Footer />
    </Router>
  );
}

export default App;
